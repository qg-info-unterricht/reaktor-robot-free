import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class AB9 extends Roboter
{

    /**
     * Hilfesmethode zum Holen des Aufzugs. 
     */
    public void holeAufzug() {
        if(!istVorne("Schalter")) {
            dreheUm();
        }
        benutze("Schalter");
    }

    /*#
     * Aufgabe 3: Drehe Roboter
     */

    public void dreheRoboter(int richtung) {
       // Hier kommt dein Quelltext hin
    }

    /*#
     * Aufgabe 4: Laufe zu X-Position
     */
    public void laufeZuXPos(int x) {
       //Hier kommt dein Quelltext hin
    }

    /*#
     * Aufgabe 5: Fahre Aufzug
     */
    public void fahreAufzug(int stockwerke, boolean abwaerts) {
        // Hier kommt dein Quelltext hin
    }

    /*#
     * Aufgabe 6: Fahre ins Stockwerk
     */
    

    /*#
     * Aufgabe 7: Bombe sichern
     */
    
    
    /*#
     * Aufgabe 8: Sprengen
     */
    
    
    /*#
     * Aufgabe 9: Legen  
     * Hier darfst du mal ganz alleine eine Methode mit Parametern implementieren ;-)
     */
   
    
    /*#
     * Platz fuer weitere Methode(n), die du eventuell fuer den Einsatz 9 benoetigst:
     */

}